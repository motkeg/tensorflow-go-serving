provider "google" {
  version = "2.0.0"
  credentials = "${file("../creds/main-dl.json")}"
  project     = "main-dl"
  region      = "us-central1"
}

provider "google-beta" {
  version = "2.0.0"
  credentials = "${file("../creds/main-dl.json")}"
  project     = "main-dl"
  region      = "us-central1"
}